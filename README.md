# Apuntes y Falacias

En este proyecto iré escribiendo una serie de guías o análisis o apuntes que se  
me vayan viniendo en gana. Normalmente estarán a medio completar o no tendrán  
sentido.  

*"El Sendero está cerrado. Fue construido por aquellos que murieron y los  
Muertos lo guardan. El Sendero está cerrado. Ahora debes morir."*  
(Rey de los Muertos)