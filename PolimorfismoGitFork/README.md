# Polimorfismo Y GitFork (Implementaciones Genéricas)

**Información**  
    - Autor: Stefano Vidaurre Olite  
    - Fecha: 1/11/2018  
    - V0.0.2 

## Introducción
La idea que hay tras este análisis es la de encontrar la manera de implementar  
una solución genérica que permita, mediante polimorfismo, la administración  
(CRUD) de un conjunto de recursos que en su base son iguales pero con la  
posibilidad extender sus propiedades (modelo de datos) pero no sus  
funcionalidades.  

El análisis se basa en implementaciones sin dependencia del lenguaje, aunque  
para el tratamiento del polimorfismo en la BBDD solo esta probado a nivel  
practico con el ORM *NHibernate (Hibernate)*.  

Ademas de utilizar el polimorfismo para la implementación de una solución  
genérica, también sera necesario el uso de GitFork para la implementación de  
la solución especifica para cada caso.  

~~~ 
    Ejemplo: Imaginemos que queremos desarrollar una solución que nos permita  
    administrar el recurso *animales* de una PetShop.  
    La implementación mediante polimorfismo seria la encargada de que con un  
    mismo desarrollo podamos gestionar por ejemplo perros, gatos y caballos  
    como si fueran el mismo recurso.  
    Por otro lado, mediante *forks* del proyecto (GitFork), podemos implementar  
    diferentes soluciones para distintos clientes que quieran usar nuestras  
    herramienta.  
    Tan solo nos tendremos que preocupar de implementar las entidades  
    especificas para cada cliente en cada caso y los cambios del núcleo se  
    propagaran fácilmente por los *forks*.  
~~~

~~~
    Nota: Se da por hecho que el patrón que se va a seguir para implementar  
    esta solución es MVC o cualquier derivado MV\*.  
    También se da por hecho que el diseño se basara en DDD. Se recomienda que  
    se siga la norma un dominio una solución (Microservicios), pero solo es una  
    recomendación para simplificar.  
~~~

## Polimorfismo  
La idea que hay tras el uso del polimorfismo para la administración de recursos  
es la de reducir el numero de tiempo invertido, servicios y controladores  
implementados para desarrollar la solución CRUD de varios recursos que en  
esencia son iguales, solo que difieren en peculiaridades en el modelo de datos.  

Por ello lo primero es definir una super-clase que hará de base, de la cual el  
resto de subclases heredaran. Esta super-clase será gestionada por el  
controlador, el servicio y el repositorio genéricos. En el caso de los DTO's  
correspondientes a estas entidades se implementaran de la misma manera, usa  
super-clase de la que heredaran cada uno de los DTO's particulares (*Para el  
caso de los ViewModel dado que son personalizaciones muy concretas habrá que  
definir un nuevo controlador y seguramente un nuevo servicio \[Pendiente de  
definir mejor este caso\])*.  

Dado que haremos uso del AutoMapper para pasar de Entidad/DTO sera necesario  
definir los GetDTOType y GetEntityType respectivamente para saber en cada  
momento a que clase debe de mapear el AutoMapper. Para gestionar la  
correspondencia Entity <-> DTO lo que se usa es el valor numérico de la  
propiedad tipo que va ligada en los DTO a una enumeración. Dicha enumeración  
deberá de estar vitaminada para poder devolver que entidad y que DTO  
corresponden para cada tipo (En C# se usan los atributos).  

Por otro lado en controlador deberá de instanciar una factoría la cual se  
encargue de crear a partir de un Json dado una instancia de la clase que  
corresponda con el tipo especificado en el Json.  

Para el la implementación del repositorio tenemos que tener en cuenta que es  
posible que cada instancia de éste, esté ligada a una conexión, con lo que tener  
todo el dominio en la misma BBDD facilitara tanto los mapeos como la  
implementación del repositorio ya que solo implementando uno puedes acceder a  
todas las diferentes instancias.  

En los mapeos con el ORM, en el caso de *Hibernate* o *NHibernate*, si acudimos a  
la documentación correspondiente a *Inheritance Mapping* se explica que el  
polimorfismo se soluciona declarando las clases que heredan como *subclass* e  
indicando sobre que tabla vamos a hacer el Join.  

De este modo conseguimos que para cada versión del recurso solo necesitemos  
implementar la entidad, el DTO y si es necesario ViewModel.  

## GitFork
El polimorfismo nos proporciona que para una solución concretar poder gestionar  
recursos similares implementando lo mínimo posible, pero también podemos tener  
la necesidad de aplicar esta solución de manera genérica, adaptándonos a cada  
cliente. En el caso de hacerlo todo sobre el mismo código, si el numero de  
clientes aumenta, hará que tengamos un montón de clases de diferentes clientes  
en cada solución concreta.  

Si para cada cliente hacemos un *fork* del *core* de la solución, tan solo con  
las entidades super-clase y las clases que gestionan a estas entidades,  
lograremos separar cada solución concreta por cliente. Por otro lado,  
personalizaciones muy concretas de un cliente no afectaran al *core* de la  
solución genérica.  

Procedimientos a seguir:  
1. Implementar la solución genérica.  
2. Crear un grupo de trabajo por cliente solo con los *developers* implicados  
3. Realizar un *fork* de la solución genérica sobre el grupo de trabajo  
4. Borrar del *fork* todas las ramas menos la *master*  
5. Cargar el remoto original (Se recomienda llamarlo *upstream*)  
6. Trabajar la solución especifica sobre *origin*  
7. En caso de que se actualice el *core* hacer un *merge* de *upstream* a *origin*  

Recomendaciones:
- No se recomienda modificar el código del *core* desde un *fork*  
- No hacer nunca *merge* (pull request) de *origin* a *upstream*  
- Llamar *upstream* al remoto padre  
- Llamar *develop_\[nombreCliente\]* a la rama *develop* del *fork*  

