# Apuntes Ecosistema Microservicios

**Información**  
    - Autor: Stefano Vidaurre Olite  
    - Fecha: 1/11/2018  
    - V0.0.4  

## 0. Coleccion de estandares

1. **Primero piensa, luego actúa:**  
    Vayas a hacer cualquier tarea, da igual que sea de desarrollo o en cualquier  
    otra situación de la vida, ten como norma primero reflexionar acerca del  
    problema e intenta encontrar todas las posibles causisticas y como darles  
    solución.  
2. **Ningún commit sin descripción:**  
    Dado que estamos trabajando en equipo, todos nuestros commits (mínimo un  
    commit por cada pequeño hito dentro de la tarea) deben de ir acompañados por  
    un comentario indicando el tipo (Add, Update, Fix, Merge, Tweak) y una breve  
    descripción a modo de enunciado sobre el hito. Recordar una rama por tarea.  
3. **Documenta, documenta y documenta:**  
    La documentación en fundamental, no solo dentro de nuestro código sino con  
    readme's, wiki's, etc. Destacar que nuestro código debe de ser lo  
    suficientemente claro para que no haya necesidad de comentarlo tan solo en  
    los puntos en los que haya algoritmos o alguna funcionalidad mas oscura de  
    lo normal. Normalmente con nombres de variables explicativos y evitando  
    código spaguetti lograremos tendremos implementaciones fáciles de mantener.  
4. **Swagger, tu mejor amigo:**  
    Ahora que todo son API's y consumos de servicios implementados por terceros  
    es de vital necesidad incluir Swagger en nuestros proyectos para que se  
    encentren dentro del estándar OpenAPI. Swagger esta diseñado para que a  
    partir de nuestra implementación de un API publique a modo de WEB la  
    interfaz mínima para poder consumirla. 
5. **REST, HTTP y a funcionar:**  
    Siguiendo con el punto anterior, REST y HTTP nos dan la mezcla del patrón y  
    el protocolo a seguir para la implementación de la API. El patrón REST nos  
    permite orientar nuestra API a recursos (seremos consumidores de recursos) y  
    HTTP es el protocolo de intercambio de mensajes mas popular y adaptado  
    actualmente.
6. **Un DTO, un Objeto:**  
    Los recursos de nuestra API físicamente son implementados como DTO's. Estos  
    deben de ser planos, solamente compuestos por variables primitivas. Esto nos  
    obliga a manejar un solo tipo de recurso a la vez con lo que nuestros  
    cambios serán seguros.
7. **BBDD NO, ORM SI:**  
    Actualmente existen una gran variedad de servidores de BBDD, con lo que  
    resultaría contraproducente desarrollar aplicaciones ancladas a un único  
    servidor. Para ello están los ORM, encargados de conectar nuestra aplicación  
    con el servidor de BBDD, y proporcionarnos todo tipo de herramientas para  
    que nuestras implementaciones estén abstraídas de la comunicación con la  
    BBDD. Por ello hay que evitar usar procedimientos almacenados, restricciones  
    en BBDD entre otros y usar el ORM para estas causisticas.  
8. **En pareja se funciona mejor:**  
    A la hora de programar cada persona debe de ser totalmente auto-suficiente  
    para implementar un solución a una problema dado. Pero aun así, es mejor que  
    este problema siempre sea resuelto y testeado entre dos personas. De este  
    modo se autocorregirán los fallos de programación, de malas practicas y  
    podrán dar una solución mas global y robusta.  
9. **Reutilizar es de sabios, copiar es de vagos:**  
    No solo reutilizar tu propio código, reutilizar el código en general sea de  
    quien sea es una buena practica. La diferencia que hay entre reutilizar a  
    copiar es que para reutilizar te tienes que molestar primero en comprender  
    el código antes de implantarlo en nuestra solución.  
10. **Testea o prepara el poper:**  
    Siempre que se vaya a iniciar la implementación de una tarea o  
    funcionalidad, el primer paso después del análisis es la implementación de  
    los test y solo después de implementar los test es cuando se inicia la  
    implementación de la solución. Estos test son deducidos a partir de los  
    criterios de aceptación.  

## 1. Ecosistema Microservicios

1. **Orquestación de contenedores:** Para cualquier aplicación basada en  
microservicios es fundamental la utilización de un orquestador de  
microservicios. Dado que cada microservicio posee su modelo y sus datos para ser  
totalmente autónomo, si se quiere que la implementación sea escalable y  
desplegada en varios contenedores es necesario el orquestador.  
Un orquestador se encarga de controlar el equilibrio de la carga, el  
enrutamiento y de tener la capacidad de inicializar los contenedores, no solo a  
nivel de un host, sino a nivel de cluster (varios host anidados). La idea que  
hay detrás de un orquestador es que para el programador sea transparente la  
infraestructura (donde esta que) y permite considerar al conjunto de host(nodos)  
como un único cluster sobre el que desplegar. También se encarga de gestionar si  
un microservicio esta caido o sobrecargado, reiniciándolo o aumentando el numero  
de instancias del mismo servicio. Existen varios ejemplos como Kubernate,  
Openshift, Swarm, Mesos.  

2. **Service-Mesh:** Es una infraestructura dedicada para manejar la  
comunicación entre microservicios. La responsabilidad principal del *service  
mesh* es entregar las solicitudes del servicio A al servicio B de una manera  
confiable, segura y oportuna. La manera de funcionar del servidor mesh es de  
forma distribuida, instanciándose en cada uno de los contenedores.  
El *service mesh* cumple las siguientes funcionalidades:  
    - Capacidades básicas que aseguran la disponibilidad en la comunicación  
    entre servicios como Circuit-breaking, reintentos, timeouts, gestión de  
    errores, balanceo de carga...  
    - Descubrimiento de servicios.  
    - Capacidades de enrutamiento, enrutamiento a versiones diferentes, entornos  
    diferentes, etc.  
    - Observabilidad de métricas, monitorización, logs y trazabilidad.  
    - Seguridad a nivel de transportes (TLS) y genstion de tokens.  
    - Autentificación/Autorización.  
    - Soporte nativo para contenedores.  
    - Protocolos de comunicación entre servicios HTTP, gRPC...  
Normalmente siguen el patrón *sidecar*, asignando a cada microservicio (o Pod en  
Kubernate) un servicio a modo de proxy que se encarga de proporcionar al  
microservicio la funcionalidades de manera desacoplada (da igual como esta  
implementado el microservicio). 

3. **Microservicio:** Como ya se puede deducir, la principal característica que  
presenta esta arquitectura es el uso de contenedores, de modo que cada  
microservicio ira confinado en un contenedor.  
Un microservicio es una implementación de software capaz de gestionar de manera  
independiente un único dominio de datos. Esto quiere decir que el acceso a los  
datos del dominio es exclusivo a el microservicio en cuestión. Esto permite  
tener piezas de código desacopladas entre si. Uno de los principales problemas  
que presentan los microservicios es la coherencia entre dominios de datos  
relacionados (paquetes y maquinas productoras). Existen patrones, como el basado  
en "eventos de cambio" que tratan de paliar este problema.  
Esta implementación tan desacoplada nos permite tener porciones de código  
atómicas, las cuales son fácilmente actualizables y versionables sin afectar al  
resto de las implementaciones.  


## 2. DevOps


## 3. Integración Continua

- **Definición:** Practica de desarrollo software donde los miembros del equipo  
integran su trabajo frecuentemente, al menos una vez al día. Cada integración se  
verifica con un build automático (que incluye la ejecución de pruebas) para  
detectar errores de integración tan pronto como sea posible.

- **Buenas practicas gracias a la IC:**
    - Pipeline: Conjunto de etapas o fases por las que pasa el software de forma  
    automática. Normalmente se cumple el siguiente orden:
        1. Build: Subida del código al repositorio de versiones, este es  
        descargado por la aplicación de IC y lo compila.  
        2. Test Unitarios: Se ejecutan el conjunto de test unitarios definidos.
        3. Despliegue en DES: El código es desplegado en desarrollo para hacer  
        los test manuales.  
        4. Despliegue en PRE: El código es desplegado en un entorno similar al  
        de producción, en el se simula como sera la subida a producción y como  
        afectara.  
        5. Despliegue en PRO: El código es desplegado en el entorno de  
        producción.  
    Antes de iniciar un desarrollo lo primero que hay que definir es bajo que  
    condiciones y que criterios debe cumplir el software para pasar de un punto  
    al al siguiente en el pipeline.
    
    - Entrega frecuente: La idea es que en vez de dejar la integración de los  
    diferentes bloques de código que desarrollan los diferentes programadores de  
    un equipo para el final, se vayan haciendo pequeñas integraciones de código  
    frecuentemente. Se evitan integraciones difíciles, pesadas y costosas.  
    
    - Build automático: El programa o sistema de IC que estemos utilizando debe  
    de descargar y luego comprobar que compila el código que tenemos subido en  
    el repositorio. De esta manera nos aseguramos que lo que esta subido como  
    mínimo compila. También se puede configurar para no permitir subir nada al  
    repositorio si no se cumple la condición de que la compilación ha sido un  
    éxito.
    
    - Ejecución de pruebas: La idea es establecer un cierto conjunto de pruebas  
    para cada uno de los niveles correspondientes a los diferentes despliegues.  
    Esto nos permite no tener que hacer todos los test que hemos definido  
    (manuales y automáticos) cada vez que subimos ya que pueden tardar mucho  
    tiempo en ejecutarse todos.
    
~~~
    Todo este conjunto de ideas no solo nos permite mejorar la calidad de los  
    desarrollos, sino que también nos permite detectar los errores tan pronto  
    como sea posible. Cuanto antes detectamos el fallo, mas fácil y rapido se  
    puede solucionar, ya que el desarrollador lo tiene mas fresco. También  
    tenemos un alto control del código que esta subido en el repositorio.
~~~

- **Componentes de IC:**
    - Repositorio de versiones: Se encargara de administrar las diferentes  
    versiones del código. El equipo de desarrollo deberá de determinar que  
    estrategia se sigue a la hora de hacer uso del repositorio.
        - Desarrollo lineal: No se desarrolla por ramas, nos evitamos conflictos  
        por merge. No es recomendable ya que los errores se propagan fácilmente.  
        No es factible con la subida frecuente de código.
        
        - Desarrollo paralelo: Se trabaja con una rama principal (develop) y de  
        ella se abren distintas ramas, una por cada tarea, historia de usuario o  
        incidencia de error (hotfix) que se vaya a implementar. Una vez que la  
        tarea es implementada, probada y sea estable, se procede a integrar la  
        rama (merge) contra la rama principal.  
            - Tarea: Implementación de una funcionalidad especifica y concreta.  
            - Historia de usuario: Descripción de un requisito que debe de  
            cumplir la aplicación. Su descripción no debe de ser técnica pero si  
            breve. (Scrum)  
            - Hotfix: Parche en caliente para solucionar un error.  
        El requisito es que las ramas implementen funcionalidades muy  
        especificas para que no tengan un duración de vida muy largar (1 día).  
        Este método reduce la propagación de errores y facilita la búsqueda en  
        caso de que los haya. También nos permite una subida frecuente del  
        código sin alterar la rama principal.  
        
        - Ramas principales: Normalmente no se trabaja solo con una rama  
        principal, lo normal es tener dos o tres ramas principales:
            - Dos ramas: Se trabaja contra la rama develop, en caso del  
            desarrollo paralelo, las ramas de tareas salen de develop y cuando  
            se superan las pruebas de aceptación (validación de cliente) se  
            integra en master indicando con un tag la versión del software.
            - Tres ramas: Misma funcionalidad que con dos ramas, solo que se  
            añade una intermedia, rama de PRE, en la que recogemos las versiones  
            subidas al entorno de pre-producción.
    
    - Servidor de IC: Se encargara de vigilar el repositorio del control de  
    versiones, dependiendo de la estrategia definida. Ante un cambio descargara  
    el código, lo compilara, realizara los test automáticos definidos y si es  
    necesario desplegara la actualización en los entornos definidos. Ademas dará  
    un feedback a los desarrolladores con el resultado de la subida, indicando  
    lo errores en caso de haberlos (email, slack, etc). Un servidor de IC  
    conocido y muy utilizado en Jenkins.  

    - Servidores de despliegue: Son los servidores en los que el servidor de IC  
    desplegara las compilaciones satisfactorias. Normalmente se tienen tres  
    tipos de entornos sobre los que desplegar, entorno de desarrollo (pruebas  
    unitarias, integración y funcionales), entorno de pre-producción (pruebas de  
    carga, estrés y de aceptación) y el entorno de producción (cliente).
    
- **Tipos de test:**    
    - Pruebas Unitarias: Se encargan de comprobar si un método (Para el que  
    definamos el test) realiza de verdad la tarea para la que esta programada.  
    Es un test que comprueba la funcionalidad de manera aislada (Solo sobre un  
    método). Comprueba que para una entrada dada da la salida que esperamos y  
    sabemos de antemano. Una buena prueba unitaria no actúa sobre una base de  
    datos (la salida puede cambiar para una misma entrada). Aquí nos interesa  
    como funciona la unidad, no la interacción entre componentes (cosa que sería  
    una prueba de integración). Si una prueba unitaria falla el problema esta en  
    el código. 
    ~~~
        Ejemplo: Método Sum(a,b) con a = 2 y b = 3 la salida tiene que ser 5.  
    ~~~
    
    - Pruebas de Integración: Se encargan de verificar que los componentes de la  
    aplicación funcionan correctamente actuando en conjunto, por ejemplo la  
    conexión a la base de datos.
    
    - Pruebas Funcionales: Se trata de comprobar si el software que se ha  
    creado, si en su conjunto, cumple con la función para la que se había  
    pensado. Es decir, para una API, si con unas entradas determinadas nos dan  
    las salidas o comportamientos esperados.
    
    - Pruebas de Carga: Son un determinado de pruebas sobre el rendimiento del  
    sistema, para un determinado numero de peticiones que respuesta tiene la  
    aplicación.
    
    - Pruebas de Estrés: Son un determinado de pruebas sobre el rendimiento del  
    sistema pero llevado a situaciones extremas, la idea es saber como se  
    comporta, si es capaz de recuperarse o como trata un posible error grave.
    
    - Pruebas de Aceptación: Son pruebas que se realizan para comprobar si la  
    aplicación cumple las expectativas del cliente.


## 4. Jenkins
- **Introducción**
    Jenkins es un servidor *open source* de integración continua escrito en  
    Java. Es uno de los servidores de IC mas utilizados y su implementación es  
    muy sencilla (tan solo es necesario bajarse el .war de [Jenkins](https://jenkins.io/) ).  

    El papel que juega Jenkins dentro de la integración continua es la  
    realización de tareas de forma automatizada (Pipeline de Jenkins): 
~~~
        Checkout -> Restore -> Clean -> Build -> Test -> Publish
~~~

    En el caso de que haya un error en algún proceso del Pipeline, como por  
    ejemplo que el código una vez compilado no consiga superar algún test de  
    verificación, se notificara al *developer* o al equipo, por email, slack...  

    En caso de superar todas las etapas podemos decirle a Jenkins que despliegue  
    la nueva versión en un entorno de pre-producción o producción.  

- **Creación de una tarea**
    

## 5. Docker


## 6. Kubernate


## 7. Istio


## 8. Implementar un microservicio


## 9. Seguridad y autentificación


## 10. Dominios de datos

- **DDD**

- **Ley de Demeter, *No aceptes caramelos de desconocidos***

- - -